
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anita Verly
 */
public class Manusia {
    public String nama;
    public int umur;
    public int uang=50000; //uang yg dipegang sekarang
    public double kebahagiaan=50;
    public String skt;
//    public int durasi;
//    public int bebanKerja;
//    public String penerima;
//    public String pemberi;
//    public int uangp; //jumlah uang dari pemberi
    
   
    
    public Manusia(String nama,int umur){ //overloading
        this.nama=nama;
        this.umur=umur;
    }
    
    public Manusia(String nama, int umur, int uang){
        this.nama=nama;
        this.umur=umur;
        this.uang=uang;
    }
    
    public void rekreasi(String namaTempat){
        int biaya=namaTempat.length()*10000;
        if(uang>biaya){
            uang-=biaya;
            kebahagiaan+=namaTempat.length();
            System.out.println(nama+" berekreasi di "+namaTempat+", "+nama+" senang :)");
            }
        else{
            System.out.println(nama+" tidak mempunyai cukup uang untuk berkreasi di "+namaTempat+" :(");
        }
    }
    
    public void sakit(String skt){
        this.setSkt(skt);
        System.out.println(getNama()+" terkena penyakit "+this.getSkt()+":0");
    }
    
    public void bekerja(double durasi,double bebanKerja){
        if(umur>=18){
            double BebanKerjaTotal=durasi*bebanKerja;
            if(BebanKerjaTotal<=kebahagiaan)
            {
                kebahagiaan-=BebanKerjaTotal;
                double pendapatand=BebanKerjaTotal*10000;
                int pendapatan=(int) pendapatand;
                uang+=pendapatan;
                System.out.println(nama+" bekerja full time , total pendapatan : "+pendapatan);
            }
            else if(BebanKerjaTotal>kebahagiaan)
            {
                double durasibaru=kebahagiaan/bebanKerja;
                BebanKerjaTotal=durasibaru*bebanKerja;
                int bebanint=(int) BebanKerjaTotal;
                int pendapatan=bebanint*10000;
                uang+=pendapatan;
                System.out.println(nama+" tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : "+ pendapatan);          }
        }
        else{
            System.out.println(nama+" belum boleh bekerja karena masih di bawah umur D:");
        }
//        return null;
       
    }

//    public void beriUang(String penerima) throws UnsupportedEncodingException{
//        
//        byte[] a=penerima.getBytes("US-ASCII");
//        System.out.println(Arrays.toString(a));
//
//        int sum=0;
//        for( int i : a) {
//             sum += i;
//            }
////        System.out.println(sum);
//        int totuangp=sum*100;
//        if(totuangp>=uang){
//            int jumlahuang=totuangp+uang;
//            int bagi=jumlahuang/6000;
//            kebahagiaan+=bagi;
//             System.out.println(pemberi+" memberi uang sebanyak "+jumlahuang+" kepada "+penerima+". Mereka berdua senang :D");
//        }
//        else
//        {
//          System.out.println(pemberi+" ingin memberi uang kepada "+penerima+". Namun tidak memiliki cukup uang :'(");  
//        }
//        
//    }
//    
//    public String beriUang(String penerima, int uangp)
//    {
//        if(uangp>=uang){
//        int totuangp=uangp+uang;
//        int bagiin=totuangp/6000;
//        kebahagiaan+=bagiin;
//        //aturan maksimumnya belum
//        return this.pemberi+" memberi uang sebanyak "+uangp+" kepada "+this.penerima+", mereka berdua senang :D";
//    
//        }
//        else
//        {
//            return this.pemberi+" ingin memberi uang kepada "+this.penerima+" namun tidak memiliki cukup uang :'(";
//        }
//    }   
    
    
    
    public String toString(){
       return("Nama \t \t:"+nama+"\n"+"Umur \t \t:"+umur+"\n"+"Uang \t \t:"+uang+"\n"+"Kebahagiaan \t:"+this.kebahagiaan);
        
    }
        
            
    /**
     * @return the nama
     */
    public String getNama() {
        return nama;
    }

    /**
     * @param nama the nama to set
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     * @return the umur
     */
    public int getUmur() {
        return umur;
    }

    /**
     * @param umur the umur to set
     */
    public void setUmur(int umur) {
        this.umur = umur;
    }

    /**
     * @return the uang
     */
    public int getUang() {
        return uang;
    }

    /**
     * @param uang the uang to set
     */
    public void setUang(int uang) {
        this.uang = uang;
    }

    /**
     * @return the kebahagiaan
     */
    public double getKebahagiaan() {
        return kebahagiaan;
    }

    /**
     * @param kebahagiaan the kebahagiaan to set
     */
    public void setKebahagiaan(int kebahagiaan) {
        this.kebahagiaan = kebahagiaan;
    }

    /**
     * @return the skt
     */
    public String getSkt() {
        return skt;
    }

    /**
     * @param skt the skt to set
     */
    public void setSkt(String skt) {
        this.skt = skt;
    }
//
//    /**
//     * @return the durasi
//     */
//    public int getDurasi() {
//        return durasi;
//    }
//
//    /**
//     * @param durasi the durasi to set
//     */
//    public void setDurasi(int durasi) {
//        this.durasi = durasi;
//    }
//
//    /**
//     * @return the bebanKerja
//     */
//    public int getBebanKerja() {
//        return bebanKerja;
//    }
//
//    /**
//     * @param bebanKerja the bebanKerja to set
//     */
//    public void setBebanKerja(int bebanKerja) {
//        this.bebanKerja = bebanKerja;
//    }
//
//    /**
//     * @return the penerima
//     */
//    public String getPenerima() {
//        return penerima;
//    }
//
//    /**
//     * @param penerima the penerima to set
//     */
//    public void setPenerima(String penerima) {
//        this.penerima = penerima;
//    }
//
//    /**
//     * @return the pemberi
//     */
//    public String getPemberi() {
//        return pemberi;
//    }
//
//    /**
//     * @param pemberi the pemberi to set
//     */
//    public void setPemberi(String pemberi) {
//        this.pemberi = pemberi;
//    }
//
//    /**
//     * @return the uangp
//     */
//    public int getUangp() {
//        return uangp;
//    }
//
//    /**
//     * @param uangp the uangp to set
//     */
//    public void setUangp(int uangp) {
//        this.uangp = uangp;
//    }
//    
}
