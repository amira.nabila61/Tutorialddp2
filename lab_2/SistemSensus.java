import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author amira nabila larasati, NPM 1606825871, Kelas DDP2-F, GitLab Account: amira.nabila61
 */

public class SistemSensus {
	public static void main(String[] args) {
        // Buat input scanner baru
		Scanner input = new Scanner(System.in);
                 

		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
//                input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
//                input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		String panjang = input.nextLine();
		System.out.print("Lebar Tubuh (cm)       : ");
		String lebar = input.nextLine();
		System.out.print("Tinggi Tubuh (cm)      : ");
		String tinggi = input.nextLine();
		System.out.print("Berat Tubuh (kg)       : ");
		String berat = input.nextLine();
		System.out.print("Jumlah Anggota Keluarga: "); //how to "menjamin"?
		String makanan = input.nextLine();
		System.out.print("Tanggal Lahir(DD-MM-YY): ");
		String tanggalLahir = input.nextLine();
                System.out.print("Catatan Tambahan       : ");
                String y = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		String jumlahcetakan = input.nextLine();

                
        //TODO bagian ngesplit tanggal tahun
                String ngesplit = tanggalLahir;
                String[] parts = ngesplit.split("-");
                String part1 = parts[0]; // 004
                String part2 = parts[1]; // 034556
                String part3= parts[2];
                        
		//TODO bagian parse
                double panjangp= Double.parseDouble(panjang);
                double lebarp= Double.parseDouble(lebar);
                double tinggip= Double.parseDouble(tinggi);
                double beratp= Double.parseDouble(berat);
                int makananp=Integer.parseInt(makanan);
                int part1p=Integer.parseInt(part1);
                int part2p=Integer.parseInt(part2);
                int part3p=Integer.parseInt(part3);
                int jumlahcetakanp=Integer.parseInt(jumlahcetakan);
                
		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
                
                double tinggib=tinggip/100;
                double panjangb=panjangp/100;
                double lebarb=lebarp/100;
                double rasiob=panjangb*lebarb*tinggib;
                double rasio = beratp/rasiob;
                int myrasio= (int) rasio;
                
                
                
		for (int i=1; i<=jumlahcetakanp; i++) {
                        System.out.println("\n");
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan "+i+ " dari " + jumlahcetakanp + " untuk: ");
			String penerima = input.nextLine();
                        String penerimab= penerima.toUpperCase();
                        // Lakukan baca input lalu langsung jadikan uppercase
                       
                       
                        
				// TODO menjamin beberapa syarat yang berlaku 
                        if (beratp<=150 && tinggip<=250 && panjangp<=250 && makananp <=20 && jumlahcetakanp<=99){
                            beratp=beratp;
                            tinggip=tinggip;
                            panjangp=panjangp;
                            makananp=makananp;
                            jumlahcetakanp=jumlahcetakanp;
          
                            }
                        else if(part3p>1000 && part3p<2018){
                            part3p=part3p;
                        }
                        else{
                                System.out.println("error");
                                   input.close();}
                        
                       
//                  

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
		
                System.out.println("DATA SIAP DICETAK UNTUK "+penerimab);
		System.out.println("-----------------------");
                System.out.println(nama+" - "+alamat);
                System.out.println("Lahir pada tanggal "+part1p+"-"+part2p+"-"+part3p);
                System.out.println("Rasio Berat Per Volume  = "+myrasio+" kg/m3");
                if (y.isEmpty()){y="tidak ada catatan tambahan";}//kenapa kalo y="" hasilnya jd kosong, tp isEmpty=benar
                else{}
                System.out.println("Catatan: "+y);
		}


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)



		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		//String nomorKeluarga = "";

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		//... anggaran = (...) (...);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		//... tahunLahir = .....; // lihat hint jika bingung
		//... umur = (...) (...);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)





		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		//String rekomendasi = "";
		//.....;

		input.close();
	}
}
